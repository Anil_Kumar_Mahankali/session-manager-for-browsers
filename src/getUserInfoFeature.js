import {inject} from 'aurelia-dependency-injection';
import GetAccessTokenFeature from './getAccessTokenFeature';
import LoginFeature from './loginFeature';
import IdentityServiceSdk from 'identity-service-sdk';

/**
 * @class {GetUserInfoFeature}
 */
@inject(GetAccessTokenFeature,LoginFeature,IdentityServiceSdk)
class GetUserInfoFeature {

    _getAccessTokenFeature:GetAccessTokenFeature;
    _loginFeature:LoginFeature;
    _identityServiceSdk:IdentityServiceSdk;

    constructor(getAccessTokenFeature:GetAccessTokenFeature,
                loginFeature:LoginFeature,
                identityServiceSdk:IdentityServiceSdk) {

        if (!getAccessTokenFeature) {
            throw 'getAccessTokenFeature required';
        }
        this._getAccessTokenFeature = getAccessTokenFeature;

        if (!loginFeature) {
            throw 'loginFeature required';
        }
        this._loginFeature = loginFeature;

        if (!identityServiceSdk) {
            throw 'identityServiceSdk required';
        }
        this._identityServiceSdk = identityServiceSdk;

    }

    /**
     * Gets userInfo for the active session. If an active session doesn't exist or is expired, initiates a login flow
     * @returns {Promise<OidcUserInfo>}
     */
    execute():Promise<OidcUserInfo> {

        return this._getAccessTokenFeature
            .execute()
            .then((accessToken) => this._identityServiceSdk.getUserInfo(accessToken))
            .catch((response) => this._loginFeature.execute());

    }

}

export default GetUserInfoFeature;
