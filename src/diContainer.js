import {Container} from 'aurelia-dependency-injection';
import localforage from 'localforage';
import IdentityServiceSdk from 'identity-service-sdk';
import AccessTokenRefreshService from './accessTokenRefreshService';
import GetAccessTokenFeature from './getAccessTokenFeature';
import GetUserInfoFeature from './getUserInfoFeature';
import LocationAdapter from './locationAdapter';
import LoginUrlFactory from './loginUrlFactory';
import LoginFeature from './loginFeature';
import LogoutFeature from './logoutFeature';
import StorageAdapter from './storageAdapter';
import SessionManagerConfig from './sessionManagerConfig';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    constructor(config:SessionManagerConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(SessionManagerConfig, config);
        this._container.registerInstance(IdentityServiceSdk, new IdentityServiceSdk(config.identityServiceSdkConfig));
        this._container.registerInstance(localforage, localforage);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AccessTokenRefreshService);
        this._container.autoRegister(GetAccessTokenFeature);
        this._container.autoRegister(GetUserInfoFeature);
        this._container.autoRegister(LocationAdapter);
        this._container.autoRegister(LoginUrlFactory);
        this._container.autoRegister(LoginFeature);
        this._container.autoRegister(LogoutFeature);
        this._container.autoRegister(StorageAdapter);

    }

}
