import {inject} from 'aurelia-dependency-injection';
import uri from 'uri';
import LocationAdapter from './locationAdapter';
import localforage from 'localforage';

/**
 * @class {StorageAdapter}
 */
@inject(LocationAdapter, localforage) class StorageAdapter {

    _locationAdapter:LocationAdapter;
    _localforage:localforage;

    constructor(locationAdapter:LocationAdapter,
                localforage:localforage) {

        if (!uri) {
            throw 'uri required';
        }

        if (!locationAdapter) {
            throw 'locationAdapter required';
        }
        this._locationAdapter = locationAdapter;

        if (!localforage) {
            throw 'localforage required';
        }
        this._localforage = localforage;

    }

    /**
     * Retrieves the current access_token from browser storage or the current URL.
     * @returns {Promise<string>}
     */
    getAccessToken() {

        return this._tryGetAccessTokenFromCurrentUrlAndConsume()
            .catch(() => this._tryGetAccessTokenFromLocalStorage());

    }

    /**
     * Sets the current access_token in local storage.
     * @param {string} accessToken
     */
    setAccessToken(accessToken:string) {

        return this._localforage.setItem('accessToken', accessToken);

    }

    _tryGetAccessTokenFromCurrentUrlAndConsume() {

        return new Promise((resolve, reject) => {

            const originalUrl = uri(this._locationAdapter.href);
            const originalHash = originalUrl.fragment();
            const originalHashAsUrl = uri(originalHash);

            // check for access token
            const accessTokenQueryParams = originalHashAsUrl.query(true).access_token;
            const accessToken =
                Array.isArray(accessTokenQueryParams) ? accessTokenQueryParams[0] : accessTokenQueryParams;

            if (accessToken) {

                const replacementHashAsUrl = uri(originalHashAsUrl).removeQuery('access_token');
                const replacementHash = `#${replacementHashAsUrl.readable()}`;

                // save access token
                this.setAccessToken(accessToken)
                    .then(() => {
                        resolve(accessToken);
                        // clear access token and replace history for security
                        this._locationAdapter.replace(replacementHash);
                    });


            }
            else {

                reject();

            }

        });

    }

    _tryGetAccessTokenFromLocalStorage():Promise < string > {

        return this._localforage.getItem('accessToken')
            .then((accessToken)=> new Promise((resolve, reject)=> {
                if (accessToken) {
                    resolve(accessToken);
                } else {
                    reject();
                }
            }));

    }

}

export default StorageAdapter;
