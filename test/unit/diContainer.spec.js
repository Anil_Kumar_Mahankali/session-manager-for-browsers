import SessionManagerConfig from '../../src/sessionManagerConfig';
import DiContainer from '../../src/diContainer';
import LoginUrlFactory from '../../src/loginUrlFactory';

const sessionManagerConfig =
    new SessionManagerConfig(
        'https://identity-service-dev.precorconnect.com',
        'https://portal.ssodev.precor.com/IdPServlet?idp_id=9lcgfn8a3xk0',
        'https://dev.precorconnect.com/customer/account/logout/'
    );

describe('DiContainer module', () => {
    describe('default export', () => {
        it('should be DiContainer class constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new DiContainer(sessionManagerConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(DiContainer));
        });
    });
});
describe('DiContainer class', () => {
    describe('get method', () => {
        it("when invoked with LoginUrlFactory should return LoginUrlFactory instance", () => {
            /*
             arrange
             */
            const objectUnderTest =
                new DiContainer(sessionManagerConfig);

            /*
             act
             */
            const actualResult =
                objectUnderTest.get(LoginUrlFactory);

            /*
             assert
             */
            expect(actualResult).toEqual(jasmine.any(LoginUrlFactory));
        })
    })
});