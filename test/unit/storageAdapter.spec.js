import StorageAdapter from '../../src/storageAdapter';
import localforage from 'localforage';

describe('StorageAdapter module', () => {
    describe('default export', () => {
        it('should be StorageAdapter class constructor', () => {

            /*
             act
             */
            const dummyLocationAdapter = {};
            const objectUnderTest =
                new StorageAdapter(dummyLocationAdapter, localforage);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(StorageAdapter));
        });
    });
});
describe('StorageAdapter class', () => {
    describe('getAccessToken method', () => {
        it('should return access token contained in url when present', (done) => {
            /*
             arrange
             */
            const expectedAccessToken = 'some.expected.accesstoken';
            const fakeLocationAdapter = {
                _href: `https://localhost/#/?access_token=${expectedAccessToken}`,
                get href() {
                    return this._href;
                },
                replace(url){
                    this._href = url;
                }
            };
            const objectUnderTest =
                new StorageAdapter(fakeLocationAdapter, localforage);

            /*
             act
             */
            const accessTokenPromise = objectUnderTest.getAccessToken();

            /*
             assert
             */

            accessTokenPromise
                .then((accessToken) => {
                    expect(accessToken).toEqual(expectedAccessToken);
                    done();
                })
                .catch((error)=> {
                    fail(error | 'promise rejected');
                    done();
                });

        });
        it('should invoke LocationAdapter.replace() with expected url', (done) => {
            /*
             arrange
             */
            const expectedUrl = '#/';
            const fakeLocationAdapter = {
                _href: `https://localhost/#/?access_token=some.dummy.accesstoken`,
                get href() {
                    return this._href;
                },
                replace(url){
                    this._href = url;
                }
            };

            spyOn(fakeLocationAdapter, 'replace');

            const objectUnderTest =
                new StorageAdapter(fakeLocationAdapter, localforage);

            /*
             act
             */
            const accessTokenPromise = objectUnderTest.getAccessToken();

            /*
             assert
             */

            accessTokenPromise
                .then(() => {
                    expect(fakeLocationAdapter.replace).toHaveBeenCalledWith(expectedUrl);
                    done();
                })
                .catch((error)=> {
                    fail(error | 'promise rejected');
                    done();
                });

        })
    })
});